
let funModule = (function (){
  return {
    isCuteMixin: function(obj) {
  obj.isCute = function() {
    return true;
  };
}, 
    singMixin: function(obj) {
  obj.sing = function() {
    console.log("Singing to an awesome tune");
  };
}
  }
})();

let orang = {
  nama: "Roofi",
}

funModule.singMixin(orang);
funModule.isCuteMixin(orang);

orang.sing()
console.log(orang.isCute());