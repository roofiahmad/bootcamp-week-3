function Animal() { }

Animal.prototype = {
  constructor: Animal,
  eat: function() {
    console.log("nom nom nom");
  }
};

function Dog() { }

// Only change code below this line

Dog.prototype=Object.create(Animal.prototype)
let beagle = new Dog();
// let black = Object.create(Animal.prototype);
// let tiny = new black(); --> giving error message black is not a constructor